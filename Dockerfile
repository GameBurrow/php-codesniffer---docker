FROM php:7.3-fpm

RUN apt-get update -y

RUN apt-get install -y \
        curl \
        libxrender1 \
        libfontconfig \
        libxtst6 \
        xz-utils \
        sudo \
        zip \
        vim \
        nano \
        git

#Install composer
RUN curl -o /tmp/composer-setup.php https://getcomposer.org/installer \
&& curl -o /tmp/composer-setup.sig https://composer.github.io/installer.sig \
# Make sure we're installing what we think we're installing!
&& php -r "if (hash('SHA384', file_get_contents('/tmp/composer-setup.php')) !== trim(file_get_contents('/tmp/composer-setup.sig'))) { unlink('/tmp/composer-setup.php'); echo 'Invalid installer' . PHP_EOL; exit(1); }" \
&& php /tmp/composer-setup.php --no-ansi --install-dir=/usr/local/bin --filename=composer --snapshot \
&& rm -f /tmp/composer-setup.*

ENV PATH="/root/.composer/vendor/bin:${PATH}"

# Install Composer stuff
RUN composer global require "squizlabs/php_codesniffer"

COPY ./custom-standards/  /root/.composer/vendor/squizlabs/php_codesniffer/src/Standards/