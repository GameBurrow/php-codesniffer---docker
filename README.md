## A simple container running PHP 7.3, Compose and PHP Codesniffer
Codesniffer: https://github.com/squizlabs/PHP_CodeSniffer

Starting the container:
```
docker-compose up -d
```

Rebuild and then start the container:
```
docker-compose up -d --build
```

Stopping the container
```
docker-compose down
```